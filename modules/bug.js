import path from 'path';

export default function BugReport (moduleOptions) {
  this.addPlugin({
    src: path.resolve(this.options.rootDir, 'plugins', 'bug.js'),
    options: { bug: 'bug!' },
  });
};
